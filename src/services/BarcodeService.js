const barcodeService = {}

barcodeService.findByBarcode = (imageBlob) => {
    const formData = new FormData()
    formData.append('file', imageBlob)
    const domain = 'https://192.168.1.140:8080'
    const endpoint =  '/physion-api/v1/barcodes'
    const url = domain + endpoint

    fetch(url, {
        method: 'POST',
        body: formData
    })
        .then(response => {
            return response.json()
        })
        .then(response => {
            if (response.code && response.code === "200") {
                console.log(response.data)
                console.log('Imagen subida correctamente')
            } else {
                alert('Error al subir la imagen(' + response.code + '):' + response.message)
                console.error('Error al subir la imagen(' + response.code + '):', response.message)
            }
        })
        .catch(error => {
            alert('Error al subir la imagen: ' +  error + ' / Endpoint: ' + url);
            console.error('Error al subir la imagen:', error)
            console.error('Endpoint: ', url)
        })
}

export default barcodeService