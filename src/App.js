import './App.css';
import CameraComponent from "./components/CameraComponent.js";
import UploadFile from "./components/UploadFile";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <CameraComponent></CameraComponent>
                <UploadFile></UploadFile>
            </header>
        </div>
    );
}

export default App;
