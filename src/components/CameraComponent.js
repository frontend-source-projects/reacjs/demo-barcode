import React, {useRef} from 'react'
import {Plugins} from '@capacitor/core'
import {CameraResultType} from '@capacitor/camera'

import barcodeService from "../services/BarcodeService"

const CameraComponent = () => {
    const buttonStyle = {
        width: '200px',
        height: '50px',
    }
    const canvasStyle = {display: 'none'}

    const canvasRef = useRef(null)

    const captureImage = async () => {
        try {
            const image = await Plugins.Camera.getPhoto({
                quality: 90,
                allowEditing: false,
                resultType: CameraResultType.Uri,
            })

            // Use 'image.webPath' to access the image path in your web code.
            // Convert the image to a Blob or base64 data, and pass it to your barcodeService.
            // For example, to get a base64 data:
            const base64Data = await fetch(image.webPath).then((r) => r.blob())
            barcodeService.findByBarcode(base64Data)
        } catch (error) {
            console.error('Error accessing camera:', error)
        }
    }

    return (
        <div>
            <button onClick={captureImage} style={buttonStyle}>
                Tomar foto
            </button>
            <canvas ref={canvasRef} style={canvasStyle}/>
        </div>
    )
}

export default CameraComponent
