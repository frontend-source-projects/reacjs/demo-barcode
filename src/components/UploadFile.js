import React, {useState} from 'react'
import barcodeService from "../services/BarcodeService";
const UploadFile = () => {
    const [file, setFile] = useState(null);

    const handleFileChange = (event) => {
        setFile(event.target.files[0]);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        barcodeService.findByBarcode(file)
    };
    return (
        <>
            <form onSubmit={handleSubmit}>
                <input type="file" onChange={handleFileChange}/>
                <button type="submit">Subir archivo</button>
            </form>
        </>
    )
}

export default UploadFile