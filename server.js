const express = require('express');
const https = require('https');
const fs = require('fs');
const path = require('path');

const app = express();
const port = 443;

const sslOptions = {
    key: fs.readFileSync(path.resolve('server.key')),
    cert: fs.readFileSync(path.resolve('server.crt'))
};

app.use(express.static('build'));

app.get('*', (req, res) => {
    res.sendFile(path.resolve('build', 'index.html'));
});

https.createServer(sslOptions, app).listen(port, () => {
    console.log(`Servidor HTTPS iniciado en el puerto ${port}`);
});