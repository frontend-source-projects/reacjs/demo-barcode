import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.example.app',
  appName: 'demo-barcode',
  webDir: 'build',
  server: {
    androidScheme: 'https',
    allowNavigation: ['https://192.168.1.140:8080']
  }
};

export default config;
